const createStack = require("./stack");

const stack = createStack();
const MY_FIRST_STRING = "My first string";
const MY_SECOND_STRING = "My second string";

test("Ensure newly created stack is empty", () => {
  expect(stack.isEmpty()).toBe(true);
});

test("Ensure adding to stack works", () => {
  stack.push(MY_FIRST_STRING);
  stack.push(MY_SECOND_STRING);
});

test("Ensure length property of stack works", () => {
  expect(stack.length).toBe(2);
});

test("Ensure peeking at stack works", () => {
  expect(stack.peek()).toBe(MY_SECOND_STRING);
});

test("Ensure popping from stack works", () => {
  var poppedValue = stack.pop();
  expect(poppedValue).toBe(MY_SECOND_STRING);
});
