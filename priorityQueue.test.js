const createPriorityQueue = require("./priorityQueue");

const queue = createPriorityQueue();
const MY_TEST_STRING_1 = "My test string 1";
const MY_TEST_STRING_2 = "My test string 2";

test("Ensure newly created queue is empty", () => {
  expect(queue.isEmpty()).toBe(true);
});

test("Ensure adding to queue works for regular priority", () => {
  queue.enqueue(MY_TEST_STRING_1);
});

test("Ensure adding to queue works for high priority", () => {
  queue.enqueue(MY_TEST_STRING_2, true);
});

test("Ensure length property of queue works", () => {
  expect(queue.length).toBe(2);
});

test("Ensure peeking works when something exists in the high priority queue", () => {
  expect(queue.peek()).toBe(MY_TEST_STRING_2);
});

test("Ensure popping from queue works", () => {
  var poppedValue = queue.dequeue();
  expect(poppedValue).toBe(MY_TEST_STRING_2);
  var poppedValue2 = queue.dequeue();
  expect(poppedValue2).toBe(MY_TEST_STRING_1);
});
