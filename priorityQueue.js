module.exports = function createPriorityQueue() {
  const lowPriorityQueue = [];
  const highPriorityQueue = [];

  return {
    enqueue(item, isHighPriority = false) {
      isHighPriority
        ? highPriorityQueue.unshift(item)
        : lowPriorityQueue.unshift(item);
    },
    dequeue() {
      if (highPriorityQueue.length !== 0) {
        return highPriorityQueue.pop();
      }
      return lowPriorityQueue.pop();
    },
    peek() {
      if (highPriorityQueue.length !== 0) {
        return highPriorityQueue[highPriorityQueue.length - 1];
      }
      return lowPriorityQueue[lowPriorityQueue.length - 1];
    },
    get length() {
      return lowPriorityQueue.length + highPriorityQueue.length;
    },
    isEmpty() {
      return lowPriorityQueue.length === 0 && highPriorityQueue.length === 0;
    }
  };
};
