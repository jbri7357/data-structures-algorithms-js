const createQueue = require("./queue");

const queue = createQueue();
const MY_TEST_STRING = "My test string";

test("Ensure newly created queue is empty", () => {
  expect(queue.isEmpty()).toBe(true);
});

test("Ensure adding to queue works", () => {
  queue.enqueue(MY_TEST_STRING);
});

test("Ensure length property of queue works", () => {
  expect(queue.length).toBe(1);
});

test("Ensure peeking at queue works", () => {
  expect(queue.peek()).toBe(MY_TEST_STRING);
});

test("Ensure popping from queue works", () => {
  var poppedValue = queue.dequeue();
  expect(poppedValue).toBe(MY_TEST_STRING);
});
